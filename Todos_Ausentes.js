// ==UserScript==
// @name         Todos Ausentes
// @namespace    G3
// @version      0.1
// @description  Un boton para poner todos ausentes en la carga de notas en un acta de examen
// @author       Ariel Eduardo Lencina Veliz 15/09/2018 alencinaveliz@gmail.com
// @match        https://gestion.guarani.unc.edu.ar/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// ==/UserScript==

this.$ = this.jQuery = jQuery.noConflict(true);

(function() {
    if(window.location.href.match(/^.+gestion.guarani.unc.edu.ar\/guarani\/[0-9.]+\/.+$/)) {
        var existe = false;
        var regex = /^form_\d+_form_ml_notas$/;

        $('input[id$="_form_ml_notas"]').each(function(){
            if(regex.exec($(this).attr('id')) !== null) {
                existe = true;
                return false
            }
        });

        if(existe) {
            var boton_descargar = $('<button/>', {
                text: 'Todos Ausentes',
                id: 'btn_ausentes',
                css: {'cursor':'pointer'},
                click: function () {
                    $('select[id$="_form_ml_notasresultado"]').each(function() {
                        $(this).val('U');
                    });
                }
            });
            agregarAlPanelHack(boton_descargar);
        }
    }
})();

function agregarAlPanelHack(element) {
  var panel = $('#panel_hack');
  if(!panel.length) {
    panel = $('<div/>', {
      id: 'panel_hack',
      css: {'top':'50%','width':'150','background-color':'c2d6d6','right':'0','position':'fixed','display':'flex','flex-direction':'column','z-index':'99999'}
    });
    $('body').append(panel);
  }
  panel.append(element);
}