// ==UserScript==
// @name         Procesar Regularidades
// @namespace    G3
// @version      1.4
// @description  try to take over the world!
// @author       Ariel Eduardo Lencina Veliz
// @match        https://gestion.guarani.unc.edu.ar/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// ==/UserScript==

this.$ = this.jQuery = jQuery.noConflict(true);

(function() {
    var fecha = "26/06/2019";
    if(window.location.href.match(/^.+gestion.guarani.unc.edu.ar\/guarani\/[0-9.]+\/.+$/)) {
        var existe = false;
        var regex = /^\d+_ef_form_\d+_form_ml_notasresultado_cursada$/;

        $('select[id$="_form_ml_notasresultado_cursada"]').each(function(){
            if(regex.exec($(this).attr('id'))) {
                existe = true;
                return false
            }
        });

        if(existe) {
            var boton_regularidades = $('<button/>', {
                text: 'Procesar Regularidad',
                id: 'btn_regularidades',
                css: {'cursor':'pointer'},
                click: function () {
                    regex = /^(\d+)_ef_form_(\d+)_form_ml_notascond_regularidad$/;
                    var aux;
                    $('select[id$="_form_ml_notascond_regularidad"]').each(function() {
                        aux = $(this).attr('id').toString().match(/^(\d+)_ef_form_(\d+)_form_ml_notascond_regularidad$/);
                        if(aux) {
                            var cond = $(this).attr('id');
                            var resu = $('#'+aux[1]+"_ef_form_"+aux[2]+"_form_ml_notasresultado_cursada").attr('id');
                            if($('#'+cond).val().localeCompare("nopar")==0 && $('#'+resu).val().localeCompare("nopar")==0) {
                                var nota_promo = $('#'+aux[1]+"_ef_form_"+aux[2]+"_form_ml_notasnota_promocion").val();
                                if(nota_promo !== "") {
                                    $('#'+resu).val("A");
                                    $('#'+cond).val("4");
                                }
                            }
                            else if($('#'+cond).val().localeCompare("nopar")!=0 && $('#'+resu).val().localeCompare("nopar")==0) {
                                if($('#'+cond).val().localeCompare("1") == 0) {
                                    $('#'+resu).val("U");
                                }
                                else if($('#'+cond).val().localeCompare("2") == 0) {
                                    $('#'+resu).val("U");
                                    $('#'+cond).val("2");
                                }
                                else if($('#'+cond).val().localeCompare("3") == 0) {
                                    $('#'+resu).val("R");
                                }
                                else if($('#'+cond).val().localeCompare("4") == 0) {
                                    $('#'+resu).val("A");
                                }
                                else if($('#'+cond).val().localeCompare("5") == 0) {
                                    $('#'+resu).val("A");
                                    $('#'+cond).val("4");
                                }
                            }
                            $('#'+aux[1]+"_ef_form_"+aux[2]+"_form_ml_notasfecha_regular").val(fecha);
                            $('#'+aux[1]+"_ef_form_"+aux[2]+"_form_ml_notaspct_asistencia_cursada").val("80");
                        }
                    });
                }
            });
            agregarAlPanelHack(boton_regularidades);
        }
    }
})();

function agregarAlPanelHack(element) {
  var panel = $('#panel_hack');
  if(!panel.length) {
    panel = $('<div/>', {
      id: 'panel_hack',
      css: {'top':'50%','width':'150','background-color':'c2d6d6','right':'0','position':'fixed','display':'flex','flex-direction':'column','z-index':'99999'}
    });
    $('body').append(panel);
  }
  panel.append(element);
}
