// ==UserScript==
// @name         Exportar a Moodle
// @namespace    G3
// @version      1.2
// @description  try to take over the world!
// @author       Ariel Lencina FCA 22/03/2020
// @match        https://gestion.guarani.unc.edu.ar/*
// @require    https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @require    https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js
// @grant        none
// ==/UserScript==

this.$ = this.jQuery = jQuery.noConflict(true);

function agregarAlPanelHack(element) {
  var panel = $('#panel_hack');
  if (!panel.length) {
    panel = $('<div/>', {
      id: 'panel_hack',
      css: { 'top': '50%', 'width': '150', 'background-color': 'c2d6d6', 'right': '0', 'position': 'fixed', 'display': 'flex', 'flex-direction': 'column', 'z-index': '99999' }
    });
    $('body').append(panel);
  }
  panel.append(element);
}

function generarCSV(lista, nombrecurso) {
  if (nombrecurso) {
    lista.forEach(function (alumnoitem) {
      alumnoitem[5] = nombrecurso;
    });
  }

  var con_comisiones = $("#chk_com").is(':checked');

  var csvdata = 'username;password;firstname;lastname;email;course1';
  if(con_comisiones) {
    csvdata += ';group1';
  }
  csvdata += '\n';

  lista.forEach(function (alumnoitem) {
    csvdata += alumnoitem[0] + ';';
    csvdata += alumnoitem[1] + ';';
    csvdata += alumnoitem[2] + ';';
    csvdata += alumnoitem[3] + ';';
    csvdata += alumnoitem[4] + ';';
    csvdata += alumnoitem[5];
    if(con_comisiones) {
      csvdata += ';' + alumnoitem[6];
    }
    csvdata += '\n';
  });

  var downloadcsv = $('<a/>', {
    id: 'downloadcsv',
    href: 'data:text/csv;charset=utf-8,' + encodeURI(csvdata),
    css: { 'display': 'none' },
    download: nombrecurso + '_' + (new Date()).getTime() + '.csv'
  });

  $('body').append(downloadcsv);

  document.getElementById("downloadcsv").click();

  $(downloadcsv).remove();
}

(function () {
  var existe = false;
  $('.item-barra-tit').each(function () {
    if ($(this).text().toLowerCase().includes('reporte de inscriptos a una com')) {
      existe = true;
      return false;
    }
  });

  if (!existe) {
    return false;
  }

  var link = $('<link/>', {
    href: 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css',
    type: 'text/css',
    rel: 'stylesheet'
  });
  $('head').append(link);

  var regexNroDoc = /^[A-Z]+\s([0-9a-zA-Z-]+)$/;

  var lista = [];

  var tabla = $('table[id^="cuerpo_js_cuadro_"]').find('tr td.ei-cuadro-cc-fondo').find('table').find('tr');

  var hayemail = false;
  var hayfecnac = false;  

  var actividad = $('select[id$="_filtroactividad"] option:selected').text().normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/ /g, "_").toLowerCase();

  $(tabla).each(function (i) {
    if (i == 0) {
      $(this).find('td').each(function () {
        if ($(this).text().toLowerCase().includes("email")) { hayemail = true; }
        else if ($(this).text().toLowerCase().includes("fecha")) { hayfecnac = true; }
      });
    }
    else {
      if (!hayemail) { return false; }

      var documento = $(this).find('td').eq(2).text().trim();
      var match = regexNroDoc.exec(documento);
      var nrodoc = match[1].trim();
      var lastname = $(this).find('td').eq(0).text().trim();
      var firstname = $(this).find('td').eq(1).text().trim();
      var comision = $(this).find('td').eq(3).text().trim();
      var email = '';
      if (hayfecnac) { email = $(this).find('td').eq(5).text().trim(); }
      else { email = $(this).find('td').eq(4).text().trim(); }

      var alumno = new Array(7);
      alumno[0] = nrodoc;
      alumno[1] = nrodoc;
      alumno[2] = lastname;
      alumno[3] = firstname;
      alumno[4] = email;
      alumno[5] = "cur";
      alumno[6] = comision;

      lista.push(alumno);
    }
  });

  var dialogX = $('<form><br><label>Nombre curso: </label><input type="text" style="z-index:10000" name="nombrecurso" value="' + actividad +
   '"><br><br><label><input type="checkbox" id="chk_com" value=""> con comisiones</label><br></form>', {
    id: 'dialogg',
    css: { 'display': 'none' }
  });

  var boton_descargar = $('<button/>', {
    text: 'Descargar csv',
    id: 'btn_descargar',
    disabled: lista.length > 0 ? false : true,
    css: { 'cursor': 'pointer' },
    click: function () {
      $(dialogX).dialog({
        modal: true,
        //autoOpen: false,
        title: "Exportar a Moodle by arielmmxii",
        width: 300,
        height: 170,
        buttons: {
          'Get CSV': function () {
            var nombrecurso = $('input[name="nombrecurso"]').val();
            generarCSV(lista, nombrecurso);
            $(this).dialog('close');
          },
          'Cancel': function () {
            $(this).dialog('close');
          }
        }
      });
    }
  });
  agregarAlPanelHack(boton_descargar);

})();