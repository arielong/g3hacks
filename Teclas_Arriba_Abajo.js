// ==UserScript==
// @name         Teclas Arriba Abajo
// @namespace    G3
// @version      0.3
// @description  Un script que permite utilizar las flechas de cursor del teclado, funcionalidad perdida en G3
// @author       Ariel Eduardo Lencina Veliz 14/09/2018 alencinaveliz@gmail.com
// @match        https://gestion.guarani.unc.edu.ar/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// ==/UserScript==

this.$ = this.jQuery = jQuery.noConflict(true);

(function() {
    if(window.location.href.match(/^.+gestion.guarani.unc.edu.ar\/guarani\/[0-9.]+\/.+$/)) {
        var existe = false;
        var regex = /^form_\d+_form_ml_notas$/;

        $('input[id$="_form_ml_notas"]').each(function(){
            if(regex.exec($(this).attr('id')) !== null) {
                existe = true;
                return false
            }
        });

        if(existe) {
            regex = /^(\d+)_ef_form_(\d+)_form_ml_notasnota$/;
            $('input[id$="_form_ml_notasnota"]').each(function(){
                if(regex.exec($(this).attr('id')) !== null) {
                    $(this).keydown(function(event){
                        var lista = $('input[id$="_form_ml_notasnota"]');
                        var regex2 = /^(\d+)_ef_form_(\d+)_form_ml_notasnota$/;
                        var match = null;
                        for(var i = 0; i < lista.length; i++) {
                            if($(lista[i]).attr('id').localeCompare($(':focus').attr('id')) == 0) {
                                if(event.keyCode == 40) {
                                    event.preventDefault();
                                    for(var j = i+1; j < lista.length; j++) {
                                        match = regex2.exec($(lista[j]).attr('id'));
                                        if(match){
                                            $(lista[j]).focus();
                                            if($(lista[i]).val().localeCompare("") == 0) {
                                                match = regex2.exec($(lista[i]).attr('id'));
                                                if(match) {
                                                    $('#' + match[1] + '_ef_form_' + match[2] + '_form_ml_notasresultado').val('U');
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                else if(event.keyCode == 38) {
                                    event.preventDefault();
                                    for(j = i-1; j >= 0; j--) {
                                        match = regex2.exec($(lista[j]).attr('id'));
                                        if(match){
                                            $(lista[j]).focus();
                                            if($(lista[i]).val().localeCompare('') == 0) {
                                                if($(lista[i]).val().localeCompare('') == 0) {
                                                    match = regex2.exec($(lista[i]).attr('id'));
                                                    if(match) {
                                                        $('#' + match[1] + '_ef_form_' + match[2] + '_form_ml_notasresultado').val('U');
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    });
                }
            });
        }
    }
})();