// ==UserScript==
// @name         Exportar Historia Academica
// @namespace    G3
// @version      0.7
// @description  try to take over the world!
// @author       Ariel Lencina FCA 01/11/2018
// @match        https://gestion.guarani.unc.edu.ar/*
// @require    https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @require    https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js
// @require    https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js
// @grant        none
// ==/UserScript==

this.$ = this.jQuery = jQuery.noConflict(true);

//Todas las materias para el Bachiller
var lista_materias = [
  '19-DT10',
  '19-00017',
  '19-00025',
  '19-00051',
  '19-00001',
  '19-00018',
  '19-00026',
  '19-00124',
  '19-00003',
  '19-00159',
  '19-00213',
  '19-00132',
  '19-00045',
  '19-00046',
  '19-00345',
  '19-00256',
  '19-00108',
  '19-00009',
  '19-00002',
  '19-00395',
  '19-00119',
  '19-00030',
  '19-00005',
  '19-00264',
  '19-00221',
  '19-00140',
  '19-00353',
  '19-00515',
  '19-00639',
  '19-00361',
  '19-00272',
  '19-00006'];

//Materias optativas para el Bachiller
var lista_materias_op = [
  '19-00119',
  '19-00030',
  '19-00005',
  '19-00264',
  '19-00221',
  '19-00140',
  '19-00353',
  '19-00515',
  '19-00639',
  '19-00361',
  '19-00272',
  '19-00006'];

var cantidad_optativas_a_cumplir = 4;

function agregarHistoria(historia) {
  var myRegexp = /^\"([^;]+)\";\"([^;]+)\s+([^;]+)\"[\s\S]+$/;
  var match = myRegexp.exec(historia);
  var nombre = '';
  var id_text = '';

  if(match !== null) {
    nombre = match[1];
    id_text = match[2] + '_' + match[3];
  }
  else return;

  if($('#alum_'+id_text).length)
    return;

  agregarAlumnoALista(id_text, nombre);

  var nueva_historia = id_text + '$$$' + historia + '$$$';
  var historias_data = localStorage.getItem('historias');

  if(historias_data === null) {
    historias_data = nueva_historia;
  }
  else {
    historias_data += nueva_historia;
  }
  localStorage.setItem('historias', historias_data);
}

function cargarHistorias() {
  var historias_data = localStorage.getItem('historias');
  if(historias_data === null)
    return;

  var myRegexp = /([^$$$]+)\$\$\$\"([^;]+)\"[^$$$]+\$\$\$/gi;

  var match = null;
  var id_text = '';
  var nombre = '';

  while ((match = myRegexp.exec(historias_data)) !== null) {
    id_text = match[1];
    nombre = match[2];

    agregarAlumnoALista(id_text, nombre);
  }
}

function agregarAlumnoALista(id, nombre) {
  var hist = $('<div/>', {
    id: 'alum_' + id,
    css: { 'border-radius':'10px',
          '-webkit-border-radius':'10px',
          '-moz-border-radius':'10px',
          'display':'inline-block',
          'padding':'2px',
          'background-color':'#EFEFEF',
          'width':'min-content',
          'text-align':'left',
          'font-size':'0.9em',
          'border':'1px solid #000' }
  });

  hist.html(nombre);
  hist.html(nombre + '&nbsp;');
  hist.append($('<a />', {id: 'rem_' + id, href: '#'}).html('[X]'));
  agregarAlPanelAbajo(hist);
  $('#rem_' + id).on('click', function(){ eliminarHistoria(id) });
}

function agregarAlPanelHack(element) {
  var panel = $('#panel_hack');
  if(!panel.length) {
    panel = $('<div/>', {
      id: 'panel_hack',
      css: {'top':'50%','width':'150','background-color':'c2d6d6','right':'0','position':'fixed','display':'flex','flex-direction':'column','z-index':'99999'}
    });
    $('body').append(panel);
  }
  panel.append(element);
}

function agregarAlPanelAbajo(element) {
  var panel_abajo = $('#panel_hack_abajo');
  if(!panel_abajo.length) {
    panel_abajo = $('<div/>', {
      id: 'panel_hack_abajo',
      css: { 'position':'fixed', 'bottom':'0', 'width':'100%', 'height':'min-content', 'background':'#4180BE' }
    });
    $('body').append(panel_abajo);
  }
  panel_abajo.append(element);
}

function eliminarHistoria(hist_id) {
	var historias_data = localStorage.getItem('historias');
  if(historias_data === null)
    return;

  if(!confirm('¿Desea realmente eliminar el alumno de la lista?'))
     return;

  var myRegexp = /([^$$$]+)\$\$\$([^$$$]+)\$\$\$/gi;

  var match = null;
  var id_text;
  var historia = '';
	var result = '';

  while ((match = myRegexp.exec(historias_data)) !== null) {
    id_text = match[1];
    historia = match[2];

    if(id_text.localeCompare(hist_id) === 0) {
      $('#alum_' + hist_id).remove();
    }
    else {
      result += id_text + '$$$' + historia + '$$$';
    }
  }
  localStorage.setItem('historias', result);
}

function esMateriaBachiller(materia) {
  var nota = ['0', '1', '2', '3'];
  return (lista_materias.includes(materia[5]) && !nota.includes(materia[7]));
}

function esMateriaBachillerOptativa(materia) {
  var nota = ['0', '1', '2', '3'];
  return (lista_materias_op.includes(materia[5]) && !nota.includes(materia[7]));
}

function ordenarPorActa(a, b) {
  if(a[9] === b[9])
    return 0;
  else
    return (a[9] < b[9])?-1:1;
}

function ordenarPorLibro(a, b) {
  if(a[8] === b[8])
    return 0;
  else
    return (a[8] < b[8])?-1:1;
}

function descargarExcel(op=null) {
  var historias_data = localStorage.getItem('historias');
  if(historias_data === null)
    return;

  var myRegexp = /([^$$$]+)\$\$\$([^$$$]+)\$\$\$/gi;
  var myRegexp2 = /^\"([^"]+)\";\"([^"]+)\";\"([^"]+)\";\"([^"]+)\";\"([^"]+)\";\"([^"]+)\";\"([^"]*)\";\"([^"]+)\";\"([^"]+)\";$/gm;

  var match = null;
  var match_linea = null;
  var historia = '';
  var lista_temp = [];
  var cantidad_optativas;

  while ((match = myRegexp.exec(historias_data)) !== null) {
    historia = match[2];
    cantidad_optativas = 0;
    while((match_linea = myRegexp2.exec(historia)) !== null) {

      if(op == 1) {
        if(!esMateriaBachiller(match_linea))
          continue;
        if(esMateriaBachillerOptativa(match_linea)) {
          if(cantidad_optativas < cantidad_optativas_a_cumplir) {
          	cantidad_optativas++;
          }
          else continue;
        }
      }
      lista_temp.push(match_linea);
    }
  }

  lista_temp.sort(ordenarPorActa);
  lista_temp.sort(ordenarPorLibro);

  var tabla = $('<table />', {id: 'tabla_export'});

  for(var i=0;i<lista_temp.length;i++) {
    var fila = $('<tr />');
    for(var j = 1; j <= 9; j++) {
      fila.append($('<td />').html(lista_temp[i][j]));
    }
    tabla.append(fila);
  }

  tabla.table2excel({
    name: "Worksheet",
    filename: "export_" + (new Date()).getTime()
	});
}

function agregarMateriaALista(lista, materia) {
  var existe = false;
  $.each(lista, function(index, fila) {
    if(fila[10].localeCompare(materia[10]) == 0 &&
       fila[11].localeCompare(materia[11]) == 0 &&
       fila[12].localeCompare(materia[12]) == 0) {
       existe = true;
       return false;
    }
  });
  if(!existe)
    lista.push(materia);
}

(function(){
  var existe = false;
  $('div.item-barra-tit').each(function() {
    if($(this).text().localeCompare('Ficha de la Persona') === 0)
      existe =  true;
  });

  if(!existe)
    return;

  var link = $('<link/>', {
    href: 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css',
    type: 'text/css',
    rel: 'stylesheet'
  });
  $('head').append(link);

  var arr = [];
  var aux = [];
  var text = '';
  var myRegexp = /^\(([^)]+)\)\s+(.+)$/;

  var cantidad_fil = $('table[id^="cuerpo_js_cuadro_"] table').find('tr').length;
  var cantidad_col = 0;
  if(cantidad_fil > 0) {
    arr = [];
    aux = [];
    text = '';
    myRegexp = /^\(([^)]+)\)\s+(.+)$/;

    for(var i = 0; i < cantidad_fil; i++) {
      cantidad_col = $('table[id^="cuerpo_js_cuadro_"] table tr').eq(i).find('td').length;
      if(i > 1 && cantidad_col > 1) {
        aux = new Array(cantidad_col+1);
        for(var j = 0; j < cantidad_col-1; j++) {
          text = $('table[id^="cuerpo_js_cuadro_"] table tr').eq(i).find('td').eq(j).html();
          aux[j] = text.trim();
          if(aux[j].localeCompare('&nbsp;') == 0)
            aux[j] = '';
        }

				var match = myRegexp.exec(aux[0]);

        if(match !== null) {
          aux[12] = match[1];
          aux[13] = match[2];
          agregarMateriaALista(arr, aux);
        }
      }
    }

    if(arr.length >= 1) {
      var apeynom = $('table tr.ei-form-fila td span').eq(1).html();
      var documento = $('table tr.ei-form-fila td span').eq(3).html();
      //var aprueba = ['4', '5', '6', '7', '8', '9', '10'];

      var boton_copiar = $('<button/>', {
        text: 'Copiar Historia',
        id: 'btn_cpy_hist',
        css: {'cursor':'pointer'},
        click: function () {
          var texto = "";
          for(var i = 0; i < arr.length; i++) {
            if(arr[i][5].localeCompare('Ausente') == 0 || arr[i][5].localeCompare('No Promocionado') == 0/* || !aprueba.includes(arr[i][4])*/)
              continue;

            texto+='"'+apeynom+'";"'+documento+'";"'+arr[i][7]+'";"'+arr[i][8]+'";"'+arr[i][12]+'";"'+arr[i][1]+'";"'+arr[i][4]+'";"_'+arr[i][11]+'";"_'+arr[i][10]+'";';
            texto+='\n';
          }
          agregarHistoria(texto);
        }
      });
      agregarAlPanelHack(boton_copiar);

      var boton_limpiar = $('<button/>', {
        text: 'Limpiar Lista',
        id: 'btn_clean_list',
        css: {'cursor':'pointer'},
        click: function () {
          $('div[id^="alum_"]').remove();
  				localStorage.removeItem('historias');
        }
      });
      agregarAlPanelHack(boton_limpiar);

      var boton_descargar = $('<button/>', {
        text: 'Descargar',
        id: 'btn_descargar',
        css: {'cursor':'pointer'},
        click: function () {
         	descargarExcel();
        }
      });
      agregarAlPanelHack(boton_descargar);

        var boton_descargar_bachiller = $('<button/>', {
        text: 'Desc Bachiller',
        id: 'btn_descargar_bachiller',
        css: {'cursor':'pointer'},
        click: function () {
         	descargarExcel(1);
        }
      });
      agregarAlPanelHack(boton_descargar_bachiller);

      cargarHistorias();
    }
  }
})();